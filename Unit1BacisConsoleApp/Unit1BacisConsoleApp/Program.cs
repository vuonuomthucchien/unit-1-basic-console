﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unit1BacisConsoleApp
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("         Xin chào"); // In chữ "Xin chào" sau đó xuống dòng.
            Console.Write(" Moi ban nhap ten cua minh: "); // In không xuống dòng.
            string name = Console.ReadLine();
            Console.WriteLine("Ten cua ban la: " + name); // Quy tắc chung trong thực hiện lệnh là lệnh bên trong thực hiện trước rồi đến lệnh bên ngoài chứa nó. Do đó chạy đến đây chương trình sẽ thực hiện lệnh Console.ReadLine() sau đó thực hiện cộng chuỗi và cuối cùng in chuỗi ra màn hình.

            //-------------

            Console.Write(" Moi ban nhap ngay sinh: ");
            string dateOfBirth = Console.ReadLine();
            Console.WriteLine(" Ngay sinh cua ban la: " + dateOfBirth); // Tương tự như trên

            //-------------

            Console.Write(" Moi ban nhap que quan: ");
            string address = Console.ReadLine();
            Console.WriteLine(" Que quan: " + address); // Tương tự như trên.

            // nâng cao 

            Console.Write(" Moi ban nhap nam sinh: ");
            string yearOfBirth = Console.ReadLine();
            //ép kiểu năm sinh từ string(chuỗi) về số
            try
            {
                int yearOfBirthInInt = int.Parse(yearOfBirth);
                int thisYear = DateTime.Now.Year;

                if (yearOfBirthInInt > thisYear)
                {
                    Console.WriteLine(" Ban da nhap sai nam "); // Tương tự như trên.
                }
                else
                {
                    int totalYear = thisYear - yearOfBirthInInt;
                    Console.WriteLine(" Ban Sinh ra cach day: " + totalYear + " nam "); // Tương tự như trên.
                }

            }
            catch (Exception)
            {
                Console.WriteLine(" Ban da nhap sai nam "); // Tương tự như trên.
                //throw;
            }

            Console.WriteLine(" An phim bat ki de thoat chuong trinh "); // Tương tự như trên.

            Console.ReadKey();
        }
    }
}
